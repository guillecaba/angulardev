import { LoginComponent } from './login.component';
import { FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';




describe ('Login', () => {
    let componente: LoginComponent;
    const servicio= new AuthService();
    
    

    beforeEach ( ()=> {
        componente = new LoginComponent(new FormBuilder(),servicio);   
    });

    it( 'Debe de crear un formulario con dos campos, email y password', ()=> {
        expect(componente.loginForm.contains('email')).toBeTruthy();
        expect( componente.loginForm.contains('password') ).toBeTruthy();

    });
    it( 'El email debe de ser obligatorio', () => {

        const control = componente.loginForm.get('email');
        control.setValue('');
        expect( control.valid ).toBeFalsy();

    });

    it( 'El password debe de ser obligatorio', () => {

        const control = componente.loginForm.get('password');
        control.setValue('');
        expect( control.valid ).toBeFalsy();

    });
    it('login: Logea correctamente', () => {
        componente.loginForm.controls['email'].setValue('guille@gmail.com');
        componente.loginForm.controls['password'].setValue('12345');
        componente.login()
        expect(servicio.logeado).toBeTruthy()
    })

    it('login: No logea', () => {
        componente.loginForm.controls['email'].setValue('guillermo@gmail.com');
        componente.loginForm.controls['password'].setValue('12345');
       
        componente.login();
        expect(servicio.logeado).toBeFalsy();
    })
})

/* import { FormularioRegister } from './formulario';
import { FormBuilder } from '@angular/forms';


describe( 'Formularios', () => {

    let componente: FormularioRegister;

    beforeEach( () => {
        componente = new FormularioRegister( new FormBuilder() );
    });

    it( 'Debe de crear un formulario con dos campos, email y password', () => {

        expect( componente.form.contains('email') ).toBeTruthy();
        expect( componente.form.contains('password') ).toBeTruthy();

    });

    it( 'El email debe de ser obligatorio', () => {

        const control = componente.form.get('email');
        control.setValue('');
        expect( control.valid ).toBeFalsy();

    });

    it( 'El email debe de ser un correo válido', () => {

        const control = componente.form.get('email');
        control.setValue('fernando@gmail.com');
        expect( control.valid ).toBeTruthy();

    });



}); */


