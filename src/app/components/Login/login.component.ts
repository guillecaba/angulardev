import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  title = 'testgcc';
  loginForm: FormGroup;
  isSubmitted  =  false;
  
  
  

  constructor(private formBuilder: FormBuilder,public _authService:AuthService) {
    this.loginForm  =  this.formBuilder.group({
      email: ['',[Validators.required,Validators.email]],
      password: ['', Validators.required]
  });
   }

   get formControls() { return this.loginForm.controls; }

login() {
  console.log(this.loginForm.value)
  this.isSubmitted = true;
  if(this.loginForm.invalid){
    return;
  }


  
  this._authService.login(this.loginForm.value).subscribe(res=>{
    console.log(res);
    if(res){
      this._authService.logeado=true;
      
    }else{
      this._authService.logeado=false;
    }
  })
  
  
}
logout(){
  this._authService.logout();
 
}

}