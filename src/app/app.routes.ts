import { Routes , RouterModule } from '@angular/router';




import { LoginComponent } from './components/Login/login.component';
import { AdminComponent } from './components/Admin/admin.component';

const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'admin', component: AdminComponent},
    {path: '**', component: LoginComponent}
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true});
