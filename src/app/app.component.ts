import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'testgcc';
  loginForm: FormGroup;
  
  

  constructor(private formBuilder: FormBuilder ) { }
  ngOnInit() {
    this.loginForm  =  this.formBuilder.group({
        email: ['', Validators.required,Validators.email],
        password: ['', Validators.required]
    });
}
login() {
  
  console.log(this.loginForm);
  
  
}
}
