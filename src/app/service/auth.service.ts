import { Injectable } from "@angular/core";
import { Observable , of  } from 'rxjs';




@Injectable()
export class AuthService { 
    users = [{"email":"guille@gmail.com","password":"12345"},{"email":"caba@gmail.com","password":"123"}]
    logeado =false;
    constructor( ){}
    login (usuario):Observable<boolean>{
        let existe=false;
        this.users.forEach(user => {
            if(user.email === usuario.email  && user.password === usuario.password){
                existe = true;
            }
        });

        if (existe){
            return of(true);
        } else{
            return of(false);
        }
    }

    public logout(){
        this.logeado = false;
      }

      public isLoggedIn(){
        return this.logeado !== false;
    
      }
}