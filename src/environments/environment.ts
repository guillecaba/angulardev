// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDpW9R3PckpOM_1-13J2gX_ZsSY7Pn3MKs",
    authDomain: "angulardev-1bfe8.firebaseapp.com",
    databaseURL: "https://angulardev-1bfe8.firebaseio.com",
    projectId: "angulardev-1bfe8",
    storageBucket: "angulardev-1bfe8.appspot.com",
    messagingSenderId: "553646091005",
    appId: "1:553646091005:web:ee548e456a3b85ae"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
