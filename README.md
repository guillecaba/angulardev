# Documentacion del proceso de integracion continua

Ambientes: Development y Production

Development desplegado en: https://angulardev-1bfe8.firebaseapp.com/#/

Production desplegado en : https://angularprod-e5b5c.firebaseapp.com/#/

Ramas del proyecto: development, homologacion y produccion.

Etapas:

-test: de desarrollo, homologacion y produccion

-build: de desarrollo, homologacion y produccion

-deploy: de desarrollo y produccion



Primeros pasos:
Se crean dos proyectos en firebase,uno para desarrollo y otro para produccion,se agrega firebase a la web app por medio de la configuracion en enviroment.ts y enviroment.prod.ts la cual vincula nuestra web app a los proyectos de desarrollo y de produccion de firebase.

Para inicializar firebase en nuestro directorio

 ```bash
    firebase login
    firebase init
```
Seleccionamos el servicio de hosting

 En .firebaserc se debe configurar a que proyecto de firebase corresponderia cada ambiente, por ejemplo development y production

 ```bash
    {  
  "projects": {
    "default": "angulardev-1bfe8",
    "development": "angulardev-1bfe8",
    "production": "angularprod-e5b5c"
  }
}
```
Luego para conseguir la $FIREBASE_DEPLOY_KEY utilizamos el siguiente comando
 

 ```bash
  firebase login
```
El cual nos sirve para crear una variable de entorno en gitlab>CI/CD Settings para el deploy

 Ir a operations -> environments dentro de gitlab y crear los ambientes de production y development y configurar el campo External Url con la url que provee el servicio de hosting de cada uno de los dos proyectos de firebase creados, un ejemplo de url es https://angulardev-1bfe8.firebaseapp.com/


Configuracion del .gitlab-ci.yml

 Nuesto proyecto cuenta con tres ramas , master, homologacion y development

 En la primera parte se define que en el entorno de CI/CD se va a utilizar node:8.15.1 y el ng-cli en la version 7.3.8

 ```bash
image: node:8.15.1
variables:
  CLI_VERSION: 7.3.8

image: trion/ng-cli-karma:${CLI_VERSION}
```

 En cache se guarda temporalmente las dependencias del proyecto , en before_script el cual define  los comandos que se van a aplicar antes de corren los jobs, se actualiza el gestor de paquetes y se instalan dependencias del entorno, luego se pasa  a instalar las dependencias del proyecto

```bash
cache:
  paths:
    - node_modules/
before_script:
  - apt-get update && apt-get install -y unzip fontconfig locales gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
  - npm install --silent
```

Stage define cuales van a ser las etapas de cada uno de los jobs que se van a ejecutar, contamos con la etapa de build, test y deploy

```bash
stages:
  - build
  - test 
  - deploy
```

La primera etapa es la de build, para la cual existen 3 distintos casos, build de development la cual se desarrolla en el entorno de desarrollo y se dispara cuando ocurre un commit dentro de desarrollo, su script ejecuta un comando de build para una version de debug. Build de homologacion se dispara cuando ocurre un commit en la rama de homologacion la cual ejecuta un comando de build de produccion. Build de produccion  la cual se dispara cuando ocurre un commit en master y realiza un build de produccion en un entorno de produccion.

```bash
build_dev:
  environment:
    name: development
  stage: build
  script:
    - echo "Run build dev"
    - npm run builddev
  only:
    - development
  artifacts:
        paths:
            - dist/testgcc
  

build_homologacion:
  stage: build
  script:
    - echo "Run build prod"
    - npm run buildprod
  only:
    - homologacion

build_prod:
  environment:
    name: production
  stage: build
  script:
    - echo "Run build prod"
    - npm run buildprod
  only:
    - master
```


 La etapa de test realiza la ejecucion de las pruebas unitarias del proyecto

```bash
test:
  stage: test
  script:
    - echo "Run unit test"
    - npm run test
```

La etapa de deploy, realizan el deploy correspondiente,deploy_dev dentro del ambiente de development corre el comando que lo despliega en el hosting de desarrollo, este es disparado cuando se realiza un commit desde la rama development.
deploy_prod es similar a deploy_dev solo que se ejecuta cuando se realiza un commit en master, se realiza en el entorno de produccion y realiza un deploy en el hosting de produccion.

```bash
deploy_dev:
  stage: deploy
  environment:
    name: development
  script:
    - npm run deploy
  only:
    - development
  artifacts:
        paths:
            - dist/testgcc


deploy_prod:
  stage: deploy
  environment:
    name: production
  script:
    - npm run deploy
  only:
    - master
```

Observacion: Commit en la rama de homologacion solo tienen etapa de test y build no de deploy, el contenido de la rama de de homologacion suele ser una version de produccion del proyecto pero a la cual todavia no se le hace deploy

En operations -> enviroments dentro del repositorio en gitlab se puede ingresar en la version live de cada uno de los ambientes para visualizar el sitio
    





















# Testgcc

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
